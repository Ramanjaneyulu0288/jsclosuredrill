

const counterFactory = () => {
    let counter = 0
    return {
        increment:function(){
            counter += 1
            return counter
        },
        decrement:function(){
            counter -= 1
            return counter
        }
    }
}


let obj = counterFactory()




console.log(obj)