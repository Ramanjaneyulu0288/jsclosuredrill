function cacheFunction(cb) {
  let cache = {};
  let count = 0;
  return function invoke(value) {
    if (Object.values(cache).includes(value)) {
      console.log(cache);
    } else {
      cb(value);
    }

    cache["value" + count] = value;
    count++;
  };
}



module.exports = cacheFunction



// What if callback takes multiple arguments ? 

//  Was asked to return cache ..


